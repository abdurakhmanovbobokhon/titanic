import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_title(name):
    name_parts = name.split(', ')
    title = name_parts[1].split(' ')[0]
    return title

def get_filled():


    median_ages = df.groupby(df['Name'].apply(get_title))['Age'].median()

    result = []

    titles = ["Mr.", "Mrs.", "Miss."]
    for title in titles:
        missing_values = df[(df['Name'].str.contains(title)) & (df['Age'].isna())].shape[0]
        median_age = round(median_ages.get(title, 0))
        result.append((title, missing_values, median_age))
    return result
